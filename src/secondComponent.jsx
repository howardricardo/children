import React from 'react'

export default class SecondComponent extends React.Component {
    render() {
        return (
            <div style={{ fontweight: this.props.fontWeight }}>{this.props.children}</div>
        )
    }
}

import Border from './border'
import React from 'react'
import FirstComponent from './firstComponent'
import SecondComponent from './secondComponent'

export default class app extends React.Component {
  render() {
    return (
    <Border title='Oi'>
      <FirstComponent color='red'>
        <strong>Sou o primeiro componente</strong>
      </FirstComponent>
      <SecondComponent fontWeight='0'>
        <strong>Sou o segundo componente</strong>
      </SecondComponent>
    </Border>
    )
  }
}

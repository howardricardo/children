import React from 'react'

export default class FirstComponent extends React.Component  {
    render() {
        return(
            <div style={{color: this.props.color}}>{this.props.children}</div>
        )
    }
}
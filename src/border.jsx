import React from 'react'
import './border.css'

const Border = ({ title, children}) => {
    return (
        <div className='Border'>
            <h1>{title}</h1>
            <div>{children}</div>
        </div>
    )
}
export default Border